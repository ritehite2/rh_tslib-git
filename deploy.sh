#!/bin/bash

# Deploy script tslib

set -e

cd $(dirname $0)

DEFAULT_DEPLOY=../deploy/rootfs
DEFAULT_CONF=../deploy/store
BIN_LOCATION=/usr/bin
LIB_LOCATION=/usr/lib
PROFILED_LOCATION=/etc/profile.d
INITD_LOCATION=/etc/init.d
RC5_LOCATION=/etc/rc5.d
NUM_ARGS=$#

if [ "$NUM_ARGS" -eq "1" ]
then
	DEPLOY_PATH="$1"
else
	DEPLOY_PATH="$DEFAULT_DEPLOY"
	echo "No path passed, using default deploy path: "$DEPLOY_PATH""
	echo ""
fi

if [ ! -e "$DEPLOY_PATH""$BIN_LOCATION" ] || [ ! -e "$DEPLOY_PATH""$LIB_LOCATION" ]
then
	echo "Required deploy path does not exist, exiting"
	exit 1
fi

if [ ! -e target_output/target ]
then
	echo "Target folder does not exist, a build is required before deploying, exiting"
	exit 1
fi

echo "#### Deploying bin files ####"
echo ""
cp target_output/target/usr/bin/* "$DEPLOY_PATH""$BIN_LOCATION"

echo "#### Deploying lib files ####"
echo ""
cp target_output/target/usr/lib/lib* "$DEPLOY_PATH""$LIB_LOCATION"
cp target_output/target/usr/lib/pkgconfig/tslib.pc "$DEPLOY_PATH""$LIB_LOCATION"/pkgconfig 
cp -r target_output/target/usr/lib/ts "$DEPLOY_PATH""$LIB_LOCATION"

echo "#### Deploying profile.d script ####"
echo ""
if [ ! -e "$DEPLOY_PATH""$PROFILED_LOCATION" ] 
then
	mkdir "$DEPLOY_PATH""$PROFILED_LOCATION"
fi
cp tslib.sh "$DEPLOY_PATH""$PROFILED_LOCATION"

echo "#### Deploying rc5.d script ####"
echo ""
if [ -e "$DEPLOY_PATH""$RC5_LOCATION"/S96ts-calibrate ] 
then
	rm "$DEPLOY_PATH""$RC5_LOCATION"/S96ts-calibrate
fi
cp ts-calibrate "$DEPLOY_PATH""$INITD_LOCATION"
ln -s ../init.d/ts-calibrate "$DEPLOY_PATH""$RC5_LOCATION"/S96ts-calibrate
#create link to file in case other programs expect it to be there
ln -s /store/ts_settings/pointercal "$DEPLOY_PATH"/etc/pointercal

echo "#### Deploying ts.conf ####"
echo ""
if [ ! -e "$DEFAULT_CONF"/ts_settings ] 
then
	mkdir "$DEFAULT_CONF"/ts_settings
fi
cp ts.conf "$DEFAULT_CONF"/ts_settings/

echo "Deploy tslib - COMPLETE"
echo ""
exit 0
