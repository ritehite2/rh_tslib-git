#!/bin/bash

# Build script for building tslibv for the rite hite gui platform
set -e

cd $(dirname $0)

#DEFAULT_TOOLCHAIN_PATH=~/ti-processor-sdk-linux-am335x-evm-05.02.00.10/linux-devkit/sysroots/x86_64-arago-linux/usr/bin
ARCHITECTURE=arm
CROSS_COMPILER=arm-linux-gnueabihf-
NUM_ARGS="$#"

echo ""

if [ $NUM_ARGS -eq 0 ]
then
	echo "No argments passed, building with default toolchain path: $DEFAULT_TOOLCHAIN_PATH"
    TOOL_CHAIN_PATH="$DEFAULT_TOOLCHAIN_PATH"
else
    TOOL_CHAIN_PATH="$1"
fi

#strong likelihood that we are in the right place if this file exists
if [ ! -e "$TOOL_CHAIN_PATH"/arm-linux-gnueabihf-gcc ]
then
    echo "Toolchain path $TOOL_CHAIN_PATH does not appear to contain the correct tools, exiting"
    echo ""
    exit 1
fi

( exec ./autogen.sh )

export PATH="$TOOL_CHAIN_PATH":"$PATH"
export ARCH="$ARCHITECTURE"
export CROSS_COMPILE="$CROSS_COMPILER"
export CFLAGS="-march=armv7-a -mtune=cortex-a8 -mfpu=neon -mthumb -mfloat-abi=hard"

( exec ./configure --prefix=/usr --exec-prefix=/target/usr --host=arm-linux-gnueabihf )

make clean; make

if [ -e ./target_output ]
then
    echo "Removing previous output"
    rm -r target_output
    mkdir target_output
else
    mkdir target_output
fi

make install DESTDIR="$(readlink -f target_output)"

echo "COMPLETE"
echo ""
exit 0
