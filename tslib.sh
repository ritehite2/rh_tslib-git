#!/bin/sh

### Touchscreen Environment Variables ###

if [ -e /dev/input/touchscreen0 ]; then
    export TSLIB_TSDEVICE=/dev/input/touchscreen0
    export TSLIB_CALIBFILE=/store/ts_settings/pointercal
	export TSLIB_CONFFILE=/store/ts_settings/ts.conf
	export TSLIB_PLUGINDIR=/usr/lib/ts
fi
